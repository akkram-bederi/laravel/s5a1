@extends('layouts.app')

@section('content')
	<h1>{{$posts->title}}</h1>
	<small>Written on {{$posts->created_at}}</small>
	<div class="my-3">
		{{$posts->body}}
	</div>
	@if(!Auth::guest())
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
	  Comment
	</button>

	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <form action="/posts/{{$post->id}}/comment" method="POST">
	        @csrf
	        <div class="form-group">
	        	<label for="content">Content:</label>
	        	<input type="text" class="form-control" name="content" id="Content">
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
	      </form>
	    </div>
	  </div>
	</div>
	@endif

	@if(count($post->comments)>0)
		<h5 class="mt-5">Comments:</h5>
		<div class="card">
			<ul class="list-group">
				@foreach($comments as $comment)
				<li class="list group-item">
					<p>{{$comment->content}}</p>
					<p>posted by:{{$comment->user->name}}</p>
					<p>posted on:{{$comment->created_at}}</p>
				</li>
				@endforeach
			</ul>
		</div>

@endsection