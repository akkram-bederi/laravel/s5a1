<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostComment extends Model
{
    public function user(){
        return $this->belongsTO('App\User)');
    }

    public function post(){
        return $this->belongsTO('App\PostComment)');
    }
}
