<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Post;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $posts=Post::where('user_id', auth()->user()->id)->get();
        // return view('home')->with('posts',$posts);

        $user_id=auth()->user()->id;

        $user=User::find($user_id);
        return view('home')->with('posts',$user->posts);
    }
}

// $posts=Post::where('user_id', auth()->user()->id)->get();
//         return view('home')->with('posts',$posts);
